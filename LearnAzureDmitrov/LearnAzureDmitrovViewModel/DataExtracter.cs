﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Research.Science.Data;
using Microsoft.Research.Science.Data.Imperative;

using LearnAzureDmitrovViewModelInterface;

namespace LearnAzureDmitrovViewModel
{
    class DataExtracter : IDataExtracter
    {
        private DataSet dataSet;

        public DataExtracter()
        {
            dataSet = null;
        }

        public List<string> ExtractData(string fileName)
        {
            List<string> res = new List<string>();

            dataSet = DataSet.Open(fileName);
            foreach (var item in dataSet.Variables)
                res.Add(item.Name + " " + item.TypeOfData.ToString());

            dataSet.Dispose();
            return res;
        }
    }
}

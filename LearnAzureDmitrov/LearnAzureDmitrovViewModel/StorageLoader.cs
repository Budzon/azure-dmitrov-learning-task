﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

using LearnAzureDmitrovViewModelInterface;

namespace LearnAzureDmitrovViewModel
{
    class StorageLoader : IStorageLoader
    {
        // Trouble! Need more names!
        static string localFileName = ".local.nc";

        private CloudStorageAccount storageAccount;
        private CloudBlobClient blobClient;

        public StorageLoader()
        {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            blobClient = storageAccount.CreateCloudBlobClient();
        }

        async public Task<List<BlobContainer>> LoadStorageStructureAsync()
        {
            List<BlobContainer> res = new List<BlobContainer>();
            List<string> names = new List<string>();

            ContainerResultSegment containerSegment;
            IEnumerable<CloudBlobContainer> containers;

            BlobResultSegment blobSegment;
            IEnumerable<IListBlobItem> blobs;

            containerSegment = await blobClient.ListContainersSegmentedAsync(null);
            containers = containerSegment.Results;

            while (containerSegment.ContinuationToken != null)
            {
                containerSegment = await blobClient.ListContainersSegmentedAsync(containerSegment.ContinuationToken);
                containers.Concat(containerSegment.Results);
            }

            foreach (var container in containers)
            {
                blobSegment = await container.ListBlobsSegmentedAsync(null);
                blobs = blobSegment.Results;

                while (blobSegment.ContinuationToken != null)
                {
                    blobSegment = await container.ListBlobsSegmentedAsync(blobSegment.ContinuationToken);
                    blobs.Concat(blobSegment.Results);
                }

                foreach (var iBlob in blobs)
                {
                    names.Add(iBlob.Uri.ToString().Split('/').Last());
                }
                res.Add(new BlobContainer(container.Name, names));
                names.Clear();
            }

            return res;
        }
        async public Task<string> LoadBlobAsync(string containerName, string blobName)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);

            await blob.DownloadToFileAsync(localFileName, System.IO.FileMode.Create);

            return localFileName;
        }
    }
}

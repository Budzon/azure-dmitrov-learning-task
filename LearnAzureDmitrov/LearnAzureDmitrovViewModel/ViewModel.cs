﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;

using LearnAzureDmitrovViewModelInterface;

namespace LearnAzureDmitrovViewModel
{
    public class ViewModel : System.Windows.Data.ListCollectionView
    {
        // Public properties used in bindings.
        public ObservableCollection<string> ListDataScheme { get; private set; }
        public bool FetchInProgress { get; private set; }

        // Public properties to work with commands.
        public ICommand WindowLoadedCmd { get { return windowLoadedCmd; } }
        public ICommand FetchButtonDownCmd { get { return fetchButtonDownCmd; } }

        private IStorageLoader storageLoader;
        private IDataExtracter dataExtracter;

        private readonly DelegateCommand windowLoadedCmd;
        private readonly DelegateCommand fetchButtonDownCmd;

        //public event PropertyChangedEventHandler PropertyChanged;

        //Constructor
        public ViewModel() : base(new ObservableCollection<BlobContainer>())
        {
            ListDataScheme = new ObservableCollection<string>();
            FetchInProgress = false;

            storageLoader = new StorageLoader();
            dataExtracter = new DataExtracter();

            windowLoadedCmd = new DelegateCommand(async _ =>
                    {
                        IEnumerable<BlobContainer> enumer = await storageLoader.LoadStorageStructureAsync();
                        foreach (var item in enumer)
                            AddNewItem(item);
                    });

            fetchButtonDownCmd = new DelegateCommand(async (blobName) =>
                    {
                        string containerName = (CurrentItem as BlobContainer).ContainerName;
                        FetchInProgress = true;
                        string fileName = await storageLoader.LoadBlobAsync(containerName, blobName as string);

                        ListDataScheme.Clear();
                        try
                        {
                            foreach (var item in dataExtracter.ExtractData(fileName))
                                ListDataScheme.Add(item);
                        }
                        catch(Exception e)
                        {
                            ListDataScheme.Add(e.ToString());
                        }

                        System.IO.File.Delete(fileName);
                        FetchInProgress = false;

                    }, (blobName) =>
                    {
                        return !FetchInProgress && (blobName != null) && (blobName as string).EndsWith(".nc");
                    });
        }
    }
}

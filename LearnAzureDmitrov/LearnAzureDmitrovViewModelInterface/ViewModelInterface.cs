﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;

using LearnAzureDmitrovViewModelInterface;

namespace LearnAzureDmitrovViewModelInterface
{
    public class BlobContainer
    {
        public string ContainerName { get; private set; }
        public List<string> BlobNames { get; private set; }

        public BlobContainer(string name, List<string> blobs)
        {
            ContainerName = name;
            BlobNames = new List<string>();

            foreach (var str in blobs)
                BlobNames.Add(str);
        }

        public override string ToString()
        {
            return ContainerName;
        }
    }

    public interface IStorageLoader
    {
        Task<List<BlobContainer>> LoadStorageStructureAsync();
        Task<string> LoadBlobAsync(string containerName, string blobName);
    }

    public interface IDataExtracter
    {
        List<string> ExtractData(string fileName);
    }

    public class DelegateCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        
        public DelegateCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public DelegateCommand(Action<object> execute,
                       Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
